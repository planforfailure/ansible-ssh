# Description

Installs an hardens SSH on Debian/Ubuntu derivatives using the following components.

- https://docs.ansible.com/ansible-core/devel/playbook_guide/playbooks_templating.html
- https://docs.ansible.com/ansible/latest/user_guide/vault.html
- https://docs.ansible.com/ansible-core/devel/playbook_guide/playbooks_variables.html

***
## Setup
Replace the variables in `defaults/main.yml`. All other sensitive variables are encrypted via `ansible-vault encrypt passwd.yml.` Ensure you have a _vault_password_file_ as per `ansible.cfg`.



## Role Variables
```
N/A
```

## Dependencies
The role expects a service to be running, all other roles meet their own dependencies.

## Installation 

```bash
$ ansible-playbook --extra-vars '@passwd.yml' deploy.yml -K
```

```yaml
---
- hosts: all
  become: yes
  gather_facts: yes
  roles:
    - roles/ansible-ssh
```
## Licence

MIT/BSD

